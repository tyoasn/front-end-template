// revslider 

var revapi10,
  tpj;
(function () {
  if (!/loaded|interactive|complete/.test(document.readyState)) document.addEventListener("DOMContentLoaded", onLoad);
  else onLoad();

  function onLoad() {
    if (tpj === undefined) {
      tpj = jQuery;
      if ("off" == "on") tpj.noConflict();
    }
    if (tpj("#rev_slider_10_1").revolution == undefined) {
      revslider_showDoubleJqueryError("#rev_slider_10_1");
    } else {
      revapi10 = tpj("#rev_slider_10_1").show().revolution({
        sliderType: "standard",
        jsFileLocation: "",
        sliderLayout: "fullwidth",
        dottedOverlay: "none",
        delay: 9000,
        navigation: {
          keyboardNavigation: "off",
          keyboard_direction: "horizontal",
          mouseScrollNavigation: "off",
          mouseScrollReverse: "default",
          onHoverStop: "on",
          touch: {
            touchenabled: "on",
            touchOnDesktop: "off",
            swipe_threshold: 75,
            swipe_min_touches: 50,
            swipe_direction: "horizontal",
            drag_block_vertical: false
          },
          arrows: {
            style: "uranus",
            enable: true,
            hide_onmobile: false,
            hide_onleave: false,
            tmp: '',
            left: {
              h_align: "left",
              v_align: "center",
              h_offset: 30,
              v_offset: 0
            },
            right: {
              h_align: "right",
              v_align: "center",
              h_offset: 30,
              v_offset: 0
            }
          },
          bullets: {
            enable: true,
            hide_onmobile: false,
            style: "hesperiden",
            hide_onleave: true,
            hide_delay: 200,
            hide_delay_mobile: 1200,
            direction: "horizontal",
            h_align: "right",
            v_align: "bottom",
            h_offset: 50,
            v_offset: 30,
            space: 10,
            tmp: ''
          }
        },
        responsiveLevels: [1240, 1024, 778, 480],
        visibilityLevels: [1240, 1024, 778, 480],
        gridwidth: [1920, 1024, 400, 480],
        gridheight: [800, 400, 300, 300],
        lazyType: "smart",
        parallax: {
          type: "mouse",
          origo: "slidercenter",
          speed: 2000,
          speedbg: 0,
          speedls: 0,
          levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50, 47, 48, 49, 50, 51, 55],
        },
        shadow: 0,
        spinner: "off",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        hideThumbsOnMobile: "off",
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        debugMode: false,
        fallbacks: {
          simplifyAll: "off",
          nextSlideOnWindowFocus: "off",
          disableFocusListener: false,
        }
      });
    }; /* END OF revapi call */

  }; /* END OF ON LOAD FUNCTION */
}()); /* END OF WRAPPING FUNCTION */

function setREVStartSize(e) {
  try {
    e.c = jQuery(e.c);
    var i = jQuery(window).width(),
      t = 9999,
      r = 0,
      n = 0,
      l = 0,
      f = 0,
      s = 0,
      h = 0;
    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
      }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
      var u = (e.c.width(), jQuery(window).height());
      if (void 0 != e.fullScreenOffsetContainer) {
        var c = e.fullScreenOffsetContainer.split(",");
        if (c) jQuery.each(c, function (e, i) {
          u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
      }
      f = u
    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
    e.c.closest(".rev_slider_wrapper").css({
      height: f
    })
  } catch (d) {
    console.log("Failure at Presize of Slider:" + d)
  }
};

function setREVStartSize(e) {
  try {
    e.c = jQuery(e.c);
    var i = jQuery(window).width(),
      t = 9999,
      r = 0,
      n = 0,
      l = 0,
      f = 0,
      s = 0,
      h = 0;
    if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
      }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
      var u = (e.c.width(), jQuery(window).height());
      if (void 0 != e.fullScreenOffsetContainer) {
        var c = e.fullScreenOffsetContainer.split(",");
        if (c) jQuery.each(c, function (e, i) {
          u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
      }
      f = u
    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
    e.c.closest(".rev_slider_wrapper").css({
      height: f
    })
  } catch (d) {
    console.log("Failure at Presize of Slider:" + d)
  }
};

// wow function

$(() => {
  new WOW().init();
});

$(() => {
  wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: false, // default
    live: true, // default
  }, );
  wow.init();
});

document.getElementById('search-bar').style.display = 'none';

function search() {
  var x = document.getElementById("search-bar");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
document.getElementById('search-bar2').style.display = 'none';

function search2() {
  var x = document.getElementById("search-bar2");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}


// owl 1

$(document).ready(function () {
  $('.owl-one').owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    autoplay: false,
    autoplayTimeout: 2000,
    responsive: {
      0: {
        items: 1,
        mouseDrag: false,
        touchDrag: true
      },
      600: {
        items: 2,
        mouseDrag: true,
        touchDrag: true
      },
      1000: {
        items: 3,
        mouseDrag: true,
        touchDrag: true
      }
    }
  });

  $('.owl-two').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    autoplay: false,
    autoplayTimeout: 2000,
    responsive: {
      0: {
        items: 3,
        mouseDrag: false,
        touchDrag: true
      },
      600: {
        items: 3,
        mouseDrag: true,
        touchDrag: true
      },
      1000: {
        items: 3,
        mouseDrag: true,
        touchDrag: true
      }
    }
  });
  $('.owl-three').owlCarousel({
    loop: false,
    margin: 0,
    nav: false,
    autoplay: false,
    autoplayTimeout: 2000,
    responsive: {
      0: {
        items: 1,
        mouseDrag: false,
        touchDrag: true
      },
      600: {
        items: 1,
        mouseDrag: true,
        touchDrag: true
      },
      1000: {
        items: 1,
        mouseDrag: true,
        touchDrag: true
      }
    }
  });

});


(function ($) {
  $('.dropdown-menu a.dropdown-toggle').on('click', function (e) {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show');

    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function (e) {
      $('.dropdown-submenu .show').removeClass("show");
    });

    return false;
  });
})(jQuery)

// scroll to top
$('#back-top a').on('click', () => {
  $('body,html').animate({
    scrollTop: 0,
  }, 600);
  return false;
});

window.onscroll = function () {
  if (pageYOffset >= 200) {
    document.getElementById('back-top').style.visibility = 'visible';
  } else {
    document.getElementById('back-top').style.visibility = 'hidden';
  }
};

// sidemenu function


var elSideMenu;
$("document").ready(function () {
  elSideMenu = $("#sidemenu-action");
  elSideMenu.addClass("sidemenu-show");
});

var nilai = 0;
var myVar;

//to use this function onmousemove="moveFunction(event)" to html component
function moveFunction(e) {
  elSideMenu.addClass("sidemenu-show").removeClass("sidemenu-hide");
  var x = e.clientX;
  var y = e.clientY;
  var z = x + y;
  nilai = z;
  //var coor = "Coordinates: (" + x + "," + y + ")";
  clearTimeout(myVar);
  myVar = setTimeout(function () {
    moving(z);
  }, 1500);
  //document.getElementById("testmove").innerHTML = coor;
}

function moving(z) {
  if (nilai == z) {
    elSideMenu.addClass("sidemenu-hide");
    elSideMenu.removeClass("sidemenu-show");
  } else {
    elSideMenu.addClass("sidemenu-show");
    elSideMenu.removeClass("sidemenu-hide");
  }
}

function hideSticky() {
  elSideMenu.addClass("sticky-hide");
  elSideMenu.removeClass("sticky-show");
  $(".hidePanelSticky").html('<i class="fa fa-backward"></i>').attr("onclick", "showSticky()");
  $("body").attr("onmousemove", "");
}

function showSticky() {
  elSideMenu.addClass("sticky-show");
  elSideMenu.removeClass("sticky-hide");
  $(".hidePanelSticky").html('<i class="fa fa-forward"></i>').attr("onclick", "hideSticky()");
}

// change icon collapse

$('[data-toggle="collapse"]').on('click', function () {
  var iconElement = $(this).find("[data-icon-in]")
  var iconIn = iconElement.attr('data-icon-in')
  var iconOut = iconElement.attr('data-icon-out')

  if (iconElement.hasClass(iconIn)) {
    iconElement.removeClass(iconIn).addClass(iconOut)
  } else {
    iconElement.removeClass(iconOut).addClass(iconIn)
  }
})

